#$ZSH_CUSTOM/functions.zsh

# Extract function
function extract()
{
    if [ -f $1 ]; then
        case $1 in
            *.7z)        7z x $1       ;;
            *.Z)         uncompress $1 ;;
            *.bz2)       bunzip2 $1    ;;
            *.gz)        gunzip $1     ;;
            *.jar)       jar xf $1     ;;
            *.rar)       rar x $1      ;;
            *.tar)       tar xvf $1    ;;
            *.tar.bz2)   tar xvjf $1   ;;
            *.tar.gz)    tar xvzf $1   ;;
            *.tbz2)      tar xvjf $1   ;;
            *.tgz)       tar xvzf $1   ;;
            *.zip)       unzip $1      ;;
            *)           echo "Don't know how to extract '$1'..." ;;
        esac
    else
        echo "'$1' is not a valid file!"
    fi
}

# Run pip without a virtualenv
function spip()
{
    sudo PIP_REQUIRE_VIRTUALENV=false pip $@
}

# Upgrade
function upgrade()
{
    sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get autoclean
    spip install -U setuptools requests virtualenv
}
