#$ZSH_CUSTOM/aliases.zsh

# Navigation

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias cdd='cd $HOME/Desktop'

# Listing

alias ls='ls -h'
alias la='ls -a'
alias ll='ls -aFl'
alias lr='ls -aFR1'
alias lt='ls -aFlt'
alias lm='CLICOLOR_FORCE=1 lt | head'
alias l1='ls -1'
alias l2='la -1'
alias l='ls -CF'

alias tr='tree'
alias ta='tree -a'
alias t1='tree -L 1'
alias t2='tree -L 2'
alias t3='tree -aL 1'
alias t4='tree -aL 2'

# Cleaning

alias cl='clear'
alias hc='rm -f $HISTFILE'

# Command

alias h='history'
alias du='du -hm'
alias py='python'
alias py2='python2'
alias py3='python3'
alias rf='rm -rf'
alias rm='rm -i'
alias less='less -FRX'
alias tree='tree -Chs'

alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'

alias bat='upower -i $(upower -e | grep 'BAT') | grep -E "state|percentage|time"'

# Docker

alias dkco='docker-compose'
alias dklc='docker ps -l'
alias dklcid='docker ps -l -q'
alias dklcip='docker inspect -f "{{.NetworkSettings.IPAddress}}" $(docker ps -l -q)'
alias dkps='docker ps'
alias dkpsa='docker ps -a'
alias dki='docker images'
alias dkrm='docker rm'
alias dkrmac='docker rm $(docker ps -a -q)'
alias dkrmlc='docker rm $(docker ps -l -q)'
alias dkrmi='docker rmi'
alias dkrmai='docker rmi -f $(docker images -q)'
alias dkrmui='docker rmi $(docker images -q -f dangling=true)'
alias dkrit='docker run -it'
alias dkst='docker stop'
alias dksta='docker stop $(docker ps -q)'
